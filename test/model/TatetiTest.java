package model;

import model.exceptions.CeldaMarcadaException;
import model.exceptions.FueraDeTableroException;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertTrue;


public class TatetiTest {
    private Tablero tablero;

    @Before
    public void setUp(){
        tablero = new Tablero();
    }
    @Test
    public void testUnTableroNuevoEstaVacio(){
        tablero = new Tablero();
        assertTrue(tablero.estaVacio());
    }

    @Test
    public void testUnaPosicionSinMarcarEstaDisponible(){
        assertTrue(tablero.posicionEstaDisponible(0,0));
    }

    @Test
    public void marcarCruzEnLaPosicionCeroCeroDeberiaHacerEso() throws CeldaMarcadaException, FueraDeTableroException {
        tablero.marcarCruz(0,0);
        assertTrue(tablero.tieneCruz(0,0));
    }

    @Test
    public void marcarCirculoEnLaPosicionCeroCeroDeberiaHacerEso() throws CeldaMarcadaException, FueraDeTableroException {
        tablero.marcarCirculo(0,0);
        assertTrue(tablero.tieneCirculo(0,0));
    }

    @Test
    public void marcarCruzEnLaPosicionCeroCeroNoDeberiaMarcarUnCirculo() throws CeldaMarcadaException, FueraDeTableroException {
        tablero.marcarCruz(0,0);
        assertFalse(tablero.tieneCirculo(0,0));
    }

    @Test(expected = CeldaMarcadaException.class)
    public void unaPosicionMarcadaNoPuedeSerOcupadaOtraVez() throws CeldaMarcadaException, FueraDeTableroException {
        tablero.marcarCruz(0,0);
        tablero.marcarCruz(0,0);
    }

    @Test
    public void siHayLineaHorizontalDeCrucesSeGanoLaPartida() throws CeldaMarcadaException, FueraDeTableroException {
        tablero.marcarCruz(0,0);
        tablero.marcarCruz(0,1);
        tablero.marcarCruz(0,2);

        assertTrue(tablero.ganoCruz());

    }

    @Test(expected = FueraDeTableroException.class)
    public void siSeIntentaMarcarAfueraDelTableroDeberiaLanzarUnaFueraDeTableroExcepcion() throws FueraDeTableroException, CeldaMarcadaException{
        tablero.marcarCirculo(0,4);
    }

    @Test
    public void hayEmpate()throws CeldaMarcadaException, FueraDeTableroException {
    /*
    *   Construccion de Tablero en situacion de empate
    *   |x|o|o|
    *   |o|o|x|
    *   |x|x|o|
    * */
        tablero.marcarCruz(0,0);
        tablero.marcarCirculo(0,1);
        tablero.marcarCirculo(0,2);
        tablero.marcarCirculo(1,0);
        tablero.marcarCirculo(1,1);
        tablero.marcarCruz(1,2);
        tablero.marcarCruz(2,0);
        tablero.marcarCruz(2,1);
        tablero.marcarCirculo(2,2);

        assertTrue(tablero.lLeno() && !tablero.hayGanador());

    }

}

// sergio.edgardo.delgadillo@gmail.com
