package model;


public class Juego{
    private Tablero tablero;
    private String[] tableroUI;
    static final String DIVISOR = "\n----------\n";
    static final String TITULO  = DIVISOR + " TA-TE-TI " + DIVISOR;

    public Juego(){
        this.tableroUI  = new String[9];
        this.tablero    = new Tablero();
    }

    public void mostrarTablero(){
        int posicion    = 1;
        String fila     = "";
        for(String celda : tableroUI){
            fila = fila + ("|" + celda + "|");
            if(posicion % 3 == 0){
                printear(fila + "\n");
                posicion = 0;
                fila = "";
            }
            posicion++;
        }
    }

    private void printear(String texto){
        System.out.print(texto);
    }

    private void inicializarTableroUI(){
        for(int i = 0; i < this.tableroUI.length; i++){
            tableroUI[i] = " ";
        }
    }

    private boolean hayGanador() {
        return this.tablero.hayGanador();
    }

    private boolean quedanMasJugadas() {
        return !this.tablero.lLeno();
    }

    public static void main(String[] args){
        Juego juego = new Juego();

        juego.inicializarTableroUI();
        juego.printear(TITULO);
        juego.mostrarTablero();

        while(!juego.hayGanador() && juego.quedanMasJugadas()){
            String input = System.console().readLine();
            int[] coordenada = new int[2];
            for(int i = 0; i < 2; i++){
                coordenada[i] = Integer.parseInt(input.split(" ")[i]);
            }
            
        }
    }


}
