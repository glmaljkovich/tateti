package model;


import model.exceptions.CeldaMarcadaException;
import model.exceptions.FueraDeTableroException;

public class Tablero {
    int casillasMarcadas;
    final int CRUZ      = 1;
    final int CIRCULO   = 2;

    int[][] representacion = new int[3][3];

    public Tablero(){
        this.casillasMarcadas = 0;
    }

    public boolean estaVacio(){
        return this.casillasMarcadas == 0;
    }

    public boolean posicionEstaDisponible(int fila, int columna) {
        return representacion[fila][columna] == 0;
    }


    public void marcarCruz(int fila, int columna) throws CeldaMarcadaException, FueraDeTableroException {
        marcarCon(CRUZ, fila, columna);
    }

    public void marcarCirculo(int fila, int columna) throws CeldaMarcadaException, FueraDeTableroException {
        marcarCon(CIRCULO, fila, columna);
    }

    private void marcarCon(int simbolo, int fila, int columna) throws CeldaMarcadaException, FueraDeTableroException {
        if(fueraDeRango(fila, columna)){
            throw new FueraDeTableroException();
        }
        int posicion = representacion[fila][columna] ;
        if(posicion != 0){
            throw new CeldaMarcadaException();
        }
        representacion[fila][columna] = simbolo;
        casillasMarcadas++;
    }

    private boolean fueraDeRango(int fila, int columna){
        return fila > 3 || fila < 0 || columna > 3 || columna < 0;
    }

    public boolean tieneCruz(int fila, int columna) {
        return this.tiene(CRUZ,fila,columna);
    }

    public boolean tieneCirculo(int fila, int columna) {
        return this.tiene(CIRCULO,fila,columna);
    }

    private boolean tiene(int simbolo, int fila, int columna){
        return representacion[fila][columna] == simbolo;
    }

    public boolean ganoCruz() {
        return this.gano(CRUZ);
    }

    public boolean ganoCirculo() {
        return this.gano(CIRCULO);
    }

    private boolean gano(int simbolo) {
        return hayLineaVertical(simbolo) || hayLineaHorizontal(simbolo) || hayDiagonal(simbolo);
    }

    private boolean hayLineaHorizontal(int simbolo) {
        return hayFilaEn(0,simbolo) || hayFilaEn(1,simbolo) || hayFilaEn(2,simbolo);

    }

    private boolean hayLineaVertical(int simbolo) {
        return hayColumnaEn(0,simbolo) || hayColumnaEn(1,simbolo)|| hayColumnaEn(2,simbolo);

    }

    private boolean hayFilaEn(int fila, int simbolo) {
        return  this.tiene(simbolo,fila,0)  && this.tiene(simbolo,fila,1) && this.tiene(simbolo,fila,2);
    }

    private boolean hayColumnaEn(int columna, int simbolo) {
        return  this.tiene(simbolo,0,columna)  && this.tiene(simbolo,1,columna) && this.tiene(simbolo,2,columna);
    }

    private  boolean hayDiagonal(int simbolo){
        return hayDiagonalPrincipal(simbolo) || hayDiagonalSecundaria(simbolo);
    }

    private boolean hayDiagonalPrincipal(int simbolo){
        return  this.tiene(simbolo,0,0)  && this.tiene(simbolo,1,1) && this.tiene(simbolo,2,2);
    }

    private boolean hayDiagonalSecundaria(int simbolo){
        return  this.tiene(simbolo,0,2)  && this.tiene(simbolo,1,1) && this.tiene(simbolo,2,0);
    }

    public boolean hayGanador() {
            return this.ganoCruz() || this.ganoCirculo();
    }

    public boolean lLeno() {
        return this.casillasMarcadas == 9;
    }
}
